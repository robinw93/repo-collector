#!/bin/bash
set -ou pipefail

# directory of the script, can be useful for locating files which are next to the script.
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Location of the main repository that contains the submodules to tag.
MAIN_REPO=`realpath ${ROOT_REPO:-${THIS_DIR}/../}`

TAG_MAJOR=false
TAG_MINOR=false
DRY_RUN=true        # dry-running by default
UPDATE_REPOS=false
STATUS=false

function main() {
  do_parse_opts $@
  print_info "Top level repository: ${MAIN_REPO}" 

  print_info "Checks OK, you have 2s to abort now!"
  sleep 2
  echo ""

  if [ ${UPDATE_REPOS} = true ]; then
    update
  fi

  if [ ${STATUS} = true ]; then
    status
  fi

  if [ ${TAG_MAJOR} = true ] || [ ${TAG_MINOR} = true ]; then

    is_dirty
    if [ $? -eq 1 ] && [ ${UPDATE_REPOS} = false ]; then
      print_info "Repositories are not clean, sure want to continue?"
      while true; do
            read -p "Please enter Y or N: " response
            case "$response" in
                [Yy]* ) break;;  # Return 0 for "Y" or "y"
                [Nn]* ) exit 1;;  # Return 1 for "N" or "n"
            esac
        done    
    fi

    for repo in $(git submodule --quiet foreach --recursive pwd); do
      create_tag ${repo}
    done

    # Tag the main repo
    create_tag ${MAIN_REPO}
  fi


  if ([ ${TAG_MAJOR} = true ] || [ ${TAG_MINOR} = true ]) && [ ${DRY_RUN} = true ]; then
    print_info "This was a dry run, run with -D or --no-dry-run to actually tag the repositories"
  fi
}

function show_help() {
  echo "Repo collection :)"

  echo "Usage:"
  echo "    $0 [-M/--tag-major] [-m/--tag-minor] [-D/--no-dry-run] [-u/--update] [-s/--status] [-p/--path]"

  echo "        *  [-h/--help]        Print this help"
  echo "        *  [-M/--tag-major]   Perform major auto tag"
  echo "        *  [-m/--tag-minor]   Perform minor auto tag"
  echo "        *  [-D/--no-dry-run]  Disable dry running"
  echo "        *  [-u/--update]      Update the repositories before tagging (pulling master ff-only)"
  echo "        *  [-s/--status]      Print the versions of the main repo and its submodules."
  echo "        *  [-p/--path]        Use this path for main repository. (Takes precedence over env var ROOT_REPO)."
  echo ""
  exit 1
}

function do_parse_opts() {
  if [ $# -eq 0 ]; then
    show_help
    exit 0
  fi

  # convert long options to shorts
  for arg in "$@"; do
    shift
    case "$arg" in
        "--help")       set -- "$@" "-h" ;;
        "--tag-major")  set -- "$@" "-M" ;;
        "--tag-minor")  set -- "$@" "-m" ;;
        "--no-dry-run") set -- "$@" "-D" ;;
        "--update")     set -- "$@" "-u" ;;
        "--status")     set -- "$@" "-s" ;;
        "--path")       set -- "$@" "-p" ;;
        *)              set -- "$@" "$arg"
    esac
  done

  OPTIND=1 # Reset in case getopts has been used previously in the shell.
  while getopts "hMmuDsp:" opt; do
      case "$opt" in
      h)
          show_help
          exit 0
          ;;
      M)  TAG_MAJOR=true
          ;;
      m)  TAG_MINOR=true
          ;;
      D)  DRY_RUN=false
          ;;
      u)  UPDATE_REPOS=true
          ;;
      s)  STATUS=true
          ;;
      p)  MAIN_REPO=${OPTARG}
      esac
  done


  shift $((OPTIND-1))

  [ "${1:-}" = "--" ] && shift

  print_info "TAG_MAJOR    = ${TAG_MAJOR}"
  print_info "TAG_MINOR    = ${TAG_MINOR}"
  print_info "DRY_RUN      = ${DRY_RUN}"
  print_info "UPDATE_REPOS = ${UPDATE_REPOS}"
  print_info "STATUS       = ${STATUS}"
}

function create_tag() {
    cd $1 

    #get highest tag number
    LAST_TAG=$(git describe --abbrev=0 --tags 2> /dev/null)

    # Set LAST_TAG to 0.0.0 if git describe did not return anything (new repo without tags)
    [ -z "${LAST_TAG}" ] && LAST_TAG="0.0.0"

    #replace . with space so can split into an array
    VERSION_BITS=(${LAST_TAG//./ })

    #get number parts and increase last one by 1
    VNUM1=${VERSION_BITS[0]}
    VNUM2=${VERSION_BITS[1]}
    VNUM3=${VERSION_BITS[2]}

    if [ ${TAG_MAJOR} = true ]; then
        VNUM1=$((VNUM1+1))
        VNUM2=0
        VNUM3=0
    elif [ ${TAG_MINOR} = true ]; then
        VNUM2=$((VNUM2+1))
        VNUM3=0
    fi

    #create new tag
    NEW_TAG="${VNUM1}.${VNUM2}.${VNUM3}"

    # Get current hash and see if it already has a tag
    NEEDS_TAG=$(git describe --contains $(git rev-parse HEAD) 2> /dev/null)

    REPO_NAME=$(basename $(realpath $1))
    # Only tag if no tag already (would be better if the git describe command above could have a silent option)
    if [ -z "${NEEDS_TAG}" ] || [ ${TAG_MAJOR} = true ]; then
        print_info "${REPO_NAME}: Requires tagging ${LAST_TAG} >> ${NEW_TAG}."
        print_info "${REPO_NAME}: Tagging ${NEW_TAG}"
        execute "git tag -a ${NEW_TAG} -m \"Tagging ${NEW_TAG}\"; git push origin ${NEW_TAG}"
    else
        print_info "${REPO_NAME}: No new commits, using ${LAST_TAG}"
    fi

    cd ${THIS_DIR}
}

function status() {
  cd ${MAIN_REPO}
  print_info "$(basename ${PWD}) @ $(git describe --tags --dirty)"

  for repo in $(git submodule --quiet foreach --recursive pwd); do
    (cd ${repo} && print_info "\t$(basename ${repo}) @ $(git describe --tags --dirty)")
  done
  cd ${THIS_DIR}
}

function is_dirty() {
  _dirty=0
  cd ${MAIN_REPO}
  for repo in $(git submodule --quiet foreach --recursive pwd); do
    cd ${repo}
    [ ! -z "$(git status -s)" ] && print_info "Untracked/uncommited changes in $(basename $(realpath $repo)). Commit before updating." && _dirty=1
  done
  cd ${MAIN_REPO}

  # cd ${MAIN_REPO}
  # [ ! -z "$(git status -s)" ] && print_info "Untracked/uncommited changes in base repo. Commit before updating." && _dirty=1
  return ${_dirty}
}

function update() {
  is_dirty
  if [[ $? -eq 1 ]]; then
    print_error "Not updating, commit changes first"
  fi

  cd ${MAIN_REPO}

  git pull
  git submodule foreach git pull origin master
  git submodule foreach git checkout master

}

function print_ok() {
  echo "[OK]"
}

function print_info() {
  while IFS= read -r line; do
    echo -e "[INFO]\t${line}"
  done <<< "$1"
}

function print_error() {
  while IFS= read -r line; do
    echo -e "[ERROR]\t${line}"
  done <<< "$1"
  exit 1
}

function execute() {
  print_info "$1"
  [[ ${DRY_RUN} = false ]] && eval "$1"
}

main $@

print_info "All done!"
echo "[OK]"