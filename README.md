# Repo Collection

## Intro
When working on projects most of the time multiple git repositories are involved.
Git submodules is a wonderful tool to collect them at one location, but what it lacks is the possibility to create tags on top level (root repository) that resolves to other tags on all the submodules (please let me know if such a thing exists).

These tags are suitable to place in a document to indicate what versions of tools/data are used. (e.g. a Software Development Environment Specification). Therefore it would be very helpful to create a set of tags to refer to from root repository to each submodule.

# Features
Run ```tag.sh --help``` for a good explanation of what this script can, but in short:
- Automatically increase versions (major or minor, -M and -m respectively)
- Pull all latest changes of the submodules and main repository (-u)
- Print the tags to put in a (release) document (-s)

## How to use
First of all one needs a repository that contains submodules. By reading this you probably already have that in place.

Place the ```tag.sh``` somewhere alongside your root repository. I've put it in a scripts directory (If you decide to place it somewhere else, you might have to update the MAIN_REPO var in the script).

### Status reporting
You can see what the current tags are by issuing:

``` 
tag.sh -s
```

That outputs something like the following:
```
[INFO]  repo-collection @ 2.0.0-dirty
[INFO]          submodule-1 @ 2.0.0
[INFO]          submodule-2 @ 2.0.0
[INFO]          submodule-3 @ 2.0.0
[INFO]  All done!
[OK]
```
This indicates that all submodules have a 2.0.0 version tag and that the root repository (repo-collection) has uncommitted changes (-dirty).

### Updating
Next one would like to create new tags, but probably on the latest state of each repository. Therefore perform an update of the root repository and all the submodules:
```
tag.sh -u
```

This yields the following:
```
[INFO]  Untracked/uncommited changes in base repo. Commit before updating.
[ERROR] Not updating, commit changes first
```
As mentioned before the repo 'repo-cllection' had uncommited changes. Commit those to succesfully update all the repositories.

### Tagging
This script supports auto tagging minor and major versions (options -m and -M respectively)

The script always performs a dry run unless stated otherwise (option -D).

Suppose we have a minor release cycle:
```
./tag.sh -m
```
That would yield something like:
```
[INFO]  submodule-1: No new commits, using 2.0.0
[INFO]  submodule-2: Requires tagging 2.0.0 >> 2.1.0.
[INFO]  submodule-2: Tagging 2.1.0
[INFO]  git tag -a 2.1.0 -m "Tagging 2.1.0"; git push origin 2.1.0
[INFO]  submodule-3: No new commits, using 2.0.0
[INFO]  repo-collection: No new commits, using 2.0.0
[INFO]  This was a dry run, run with -D or --no-dry-run to actually tag the repositories
[INFO]  All done!
[OK]
```
submodule-2 has new commits by issuing ```./tag.sh -mD``` the script would actually create the tag and push that to ```origin```.

## Disclaimer
Software contains bugs, and therefore probably also this conventient helper script.

Read before use and use with care.

That's it.

Useful? I'd think so!
